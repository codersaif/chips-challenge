package bd.edu.green.tiles;

public class TileType {

	public static final int BASE_TILE = 0;
	public static final int WALL = 1;
	public static final int CHIP = 15;

	public static final int KEY_YELLOW = 11;
	public static final int KEY_BLUE = 12;
	public static final int KEY_RED = 13;
	public static final int KEY_GREEN = 14;

	public static final int LOCK_BLUE = 90;
	public static final int LOCK_RED = 91;
	public static final int LOCK_GREEN = 92;
	public static final int LOCK_YELLOW = 89;

	public static final int GOAL_ENTRANCE = 93;
	public static final int GOAL = 94;

}
