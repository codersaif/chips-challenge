package bd.edu.green.tiles;

import org.newdawn.slick.Image;
import org.newdawn.slick.SpriteSheet;

import bd.edu.green.app.Inventory;

public class ChipTile extends Tile {

	protected boolean hasChip = true;
	protected Image background;
	
	public ChipTile(SpriteSheet s, int x, int y) {
		super(x, y);
		init(s);
	}
	
	public void  init(SpriteSheet s)
	{
		this.background =  s.getSubImage(0, 0);
		this.img = s.getSubImage(14, 2);
	}
	
	@Override
	public void draw(){
		this.background.draw(this.x, this.y);
		if(hasChip)
		this.img.draw(this.x, this.y);
	}
	
	@Override
	public void execute(){
		if(hasChip){
			Inventory.getInstance().chips--;
			hasChip = false;
		}		
	}

}
