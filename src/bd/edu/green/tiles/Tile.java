package bd.edu.green.tiles;

import org.newdawn.slick.Image;

public class Tile {

	protected Image img;
	public int x;
	public int y;
	public int accessible = 1;
	public static final int WIDTH = 48, HEIGHT = 48;

	public Tile(int x, int y) {
		this.x = x;
		this.y = y;		
	}

	public void draw() {
		this.img.draw(this.x, this.y);
	}

	public void execute() {

	}

}
