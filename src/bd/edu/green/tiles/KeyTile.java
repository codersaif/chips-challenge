package bd.edu.green.tiles;

import org.newdawn.slick.Image;
import org.newdawn.slick.SpriteSheet;

import bd.edu.green.app.Inventory;

public class KeyTile extends Tile {
	protected int color;
	protected Image background;
	protected boolean hasKey = true;

	public KeyTile(SpriteSheet s, int x, int y, int color) {
		super(x, y);
		init(s, color);
	}

	public void init(SpriteSheet s, int color) {
		this.color = color;
		this.background = s.getSubImage(0, 0);
		switch (color) {
		case Color.BLUE:
			this.img = s.getSubImage(16, 2);
			break;
		case Color.GREEN:
			this.img = s.getSubImage(18, 2);
			break;
		case Color.RED:
			this.img = s.getSubImage(15, 2);
			break;
		case Color.YELLOW:
			this.img = s.getSubImage(17, 2);
			break;
		}
	}

	@Override
	public void draw() {
		this.background.draw(this.x, this.y);
		if (hasKey)
			this.img.draw(this.x, this.y);
	}

	@Override
	public void execute() {
		if (hasKey) {
			switch (this.color) {
			case Color.BLUE:
				Inventory.getInstance().keyBlue++;
				break;
			case Color.GREEN:
				Inventory.getInstance().keyGreen++;
				break;
			case Color.RED:
				Inventory.getInstance().keyRed++;
				break;
			case Color.YELLOW:
				Inventory.getInstance().keyYellow++;
				break;
			}
		}
		this.hasKey = false;
	}

}
