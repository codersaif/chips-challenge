package bd.edu.green.tiles;

import org.newdawn.slick.SpriteSheet;

import bd.edu.green.app.Inventory;

public class GoalTile  extends Tile{

	public GoalTile(SpriteSheet s, int x, int y) {
		super(x, y);
		init(s);
	}
	public void  init(SpriteSheet s)
	{
		this.img = s.getSubImage(11, 2);
	}
	@Override
	public void execute(){
		System.out.println("Game Over");
		Inventory.movement = false;
		
			
	}		
}
