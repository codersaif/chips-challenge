package bd.edu.green.tiles;

public class Color {
	public static final int YELLOW = 1;
	public static final int GREEN = 2;
	public static final int BLUE = 3;
	public static final int RED = 4;
}
