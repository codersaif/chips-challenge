package bd.edu.green.tiles;

import org.newdawn.slick.SpriteSheet;

public class GoalEntranceTile extends Tile{

	public GoalEntranceTile(SpriteSheet s, int x, int y) {
		super(x, y);
		init(s);
	}
	public void init(SpriteSheet s)
	{
		this.accessible = 0;
		this.img = s.getSubImage(9, 2);
	}

}
