package bd.edu.green.tiles;

import org.newdawn.slick.SpriteSheet;

public class BaseTile extends Tile{

	public BaseTile(SpriteSheet s, int x, int y) {
		super(x, y);
		init(s);
		
	}
	public void  init(SpriteSheet s)
	{
		this.img = s.getSubImage(0, 0);
	}
	
	
}
