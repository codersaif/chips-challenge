package bd.edu.green.tiles;

import org.newdawn.slick.SpriteSheet;

public class WallTile extends Tile{

	public WallTile(SpriteSheet s, int x, int y) {
		super(x, y);
		init(s);		
	}
	public void  init(SpriteSheet s)
	{
		this.accessible = 0;
		this.img = s.getSubImage(21, 1);
	}

}
