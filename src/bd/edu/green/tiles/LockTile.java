package bd.edu.green.tiles;

import org.newdawn.slick.Image;
import org.newdawn.slick.SpriteSheet;

import bd.edu.green.app.Inventory;

public class LockTile extends Tile {

	protected Image background;
	protected boolean hasLock = true;
	protected int color;
	protected Inventory inventory = Inventory.getInstance();
	public LockTile(SpriteSheet s, int x, int y, int color) {
		super(x, y);
		init(s, color);
	}

	public void init(SpriteSheet s, int color) {
		this.background =  s.getSubImage(0, 0);
		this.accessible = 2;
		this.color = color;
		switch (color) {
		case Color.BLUE:
			this.img = s.getSubImage(6, 2);
			break;
		case Color.GREEN:
			this.img = s.getSubImage(8, 2);
			break;
		case Color.RED:
			this.img = s.getSubImage(5, 2);
			break;
		case Color.YELLOW:
			this.img = s.getSubImage(7, 2);
			break;
		}
	}
	
	@Override
	public void draw(){
		this.background.draw(this.x, this.y);
		if(hasLock)
		this.img.draw(this.x, this.y);
	}
	
	@Override
	public void execute(){
		System.out.println(hasLock);
		if(hasLock){
			switch (this.color) {
			case Color.BLUE:
				if(inventory.keyBlue>0){
					inventory.keyBlue--;
					this.accessible = 1;
					hasLock = false;
				}
				break;
			case Color.GREEN:
				if(inventory.keyGreen>0){
					//Inventory.keyGreen--;
					this.accessible = 1;
					hasLock = false;
				}
				break;
			case Color.RED:
				if(inventory.keyRed>0){
					inventory.keyRed--;
					this.accessible = 1;
					hasLock = false;
				}
				break;
			case Color.YELLOW:
				if(inventory.keyYellow>0){
					inventory.keyYellow--;
					this.accessible = 1;
					hasLock = false;
				}
				break;
			}
			
		}		
	}

}
