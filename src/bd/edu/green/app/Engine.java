package bd.edu.green.app;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class Engine extends BasicGameState {
	Map map;

	private int tilseWidth = 48;
	private Image img = null;
	int duration = 100;
	Player player;
	SpriteSheet s;
	Inventory inventory;

	public Engine() {

	}

	public void init(GameContainer Container, StateBasedGame arg1) throws SlickException {
		img = new Image("img/01.png");
		s = new SpriteSheet(img, tilseWidth, tilseWidth);
		map = new Map(s);
		this.inventory = Inventory.getInstance();
		this.inventory.init(s);
		this.player = new Player(s);

	}

	public void render(GameContainer arg0, StateBasedGame arg1, Graphics g) throws SlickException {
		inventory.draw(g);
		map.draw(g);
		player.draw(g);
	}

	public void update(GameContainer container, StateBasedGame arg1, int delta) throws SlickException {
		
		Inventory.getInstance().updateTime(delta);

		Input input = container.getInput();
		if(!Inventory.movement){
			return;			
		}

		if (input.isKeyDown(Input.KEY_LEFT)) {
			player.move(Direction.LEFT);
			map.moveLeft();			
		} else if (input.isKeyDown(Input.KEY_RIGHT)) {
			player.move(Direction.RIGHT);
			map.moveRight();
		} else if (input.isKeyDown(Input.KEY_UP)) {
			player.move(Direction.UP);
			map.moveUp();
		} else if (input.isKeyDown(Input.KEY_DOWN)) {
			player.move(Direction.DOWN);
			map.moveDown();
		}
	}

	public int getID() {
		return 0;
	}

}
