package bd.edu.green.app;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.SpriteSheet;

import bd.edu.green.tiles.BaseTile;
import bd.edu.green.tiles.Color;
import bd.edu.green.tiles.KeyTile;
import bd.edu.green.tiles.Tile;

public class Inventory {
	public int chips = 0;
	public static boolean movement = true;
	public static int TILE_SHOW_X = 4;

	private static Inventory instance;
	private Tile[] backgroundTiles = new Tile[8];
	private SpriteSheet spriteSheet;
	public int keyBlue = 0, keyRed = 0, keyYellow = 0, keyGreen = 0;
	
	private float timeRemain = 4 * 60.0f;
	
	private Inventory(){
		
	}

	public static Inventory getInstance() {
		if (instance == null) {
			instance = new Inventory();
		}
		return instance;
	}

	public void init(SpriteSheet s) {
		this.spriteSheet = s;
		for (int i = 0; i < backgroundTiles.length; i++) {
			int tx = Map.OFFSETX * 2 + Map.TILE_SHOW_X * Tile.WIDTH + (i % 4) * Tile.WIDTH;
			int ty = Map.OFFSETX + ((i / 4) + 2) * Tile.WIDTH;
			Tile background = new BaseTile(s, tx, ty);
			this.backgroundTiles[i] = background;
		}
	}

	public void draw(Graphics g) {
		int keyBlueTemp = this.keyBlue;
		int keyRedTemp = this.keyRed;
		int keyGreenTemp = this.keyGreen;
		int keyYellowTemp = this.keyYellow;
		Tile t;
		for (int i = 0; i < backgroundTiles.length; i++) {
			this.backgroundTiles[i].draw();
			int tx = this.backgroundTiles[i].x; 
			int ty = this.backgroundTiles[i].y; 
			if (keyGreenTemp > 0) {
				t = new KeyTile(this.spriteSheet, tx, ty, Color.GREEN);
				t.draw();
				keyGreenTemp--;
			} else if (keyBlueTemp > 0) {
				t = new KeyTile(this.spriteSheet, tx, ty, Color.BLUE);
				t.draw();
				keyBlueTemp--;
			} else if (keyRedTemp > 0) {
				t = new KeyTile(this.spriteSheet, tx, ty, Color.RED);
				t.draw();
				keyRedTemp--;
			} else if (keyYellowTemp > 0) {
				t = new KeyTile(this.spriteSheet, tx, ty, Color.YELLOW);
				t.draw();
				keyYellowTemp--;
			}
		}
		g.setColor(org.newdawn.slick.Color.gray);
		g.drawString("Time Remain: " + (int)Inventory.getInstance().timeRemain, Map.OFFSETX * 2 + Map.TILE_SHOW_X * Tile.HEIGHT,
				Map.OFFSETX + Tile.HEIGHT-24);
		g.drawString("Chips Remain: " + Inventory.getInstance().chips, Map.OFFSETX * 2 + Map.TILE_SHOW_X * Tile.HEIGHT,
				Map.OFFSETX + Tile.HEIGHT);
	}
	
	public void updateTime(float delta){
		this.timeRemain -= delta/1000;
	}

}