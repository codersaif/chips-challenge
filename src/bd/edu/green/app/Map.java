package bd.edu.green.app;

import java.awt.Point;


import org.newdawn.slick.Graphics;
import org.newdawn.slick.SpriteSheet;

import bd.edu.green.tiles.KeyTile;
import bd.edu.green.tiles.LockTile;
import bd.edu.green.tiles.Tile;
import bd.edu.green.tiles.ChipTile;
import bd.edu.green.tiles.Color;
import bd.edu.green.tiles.GoalEntranceTile;
import bd.edu.green.tiles.GoalTile;
import bd.edu.green.tiles.BaseTile;

import bd.edu.green.tiles.TileType;
import bd.edu.green.tiles.WallTile;

public class Map {
	public static final int MOVE_STEP = 48;
	private SpriteSheet s;
	BaseTile pTiles;
	WallTile wall;
	ChipTile chip;

	KeyTile yellowKey, greenKey, redKey, blueKey;
	LockTile redLock, greenLock, blueLock, yellowLock;

	private Tile[][] tiles = new Tile[25][26];
	private int[][] tilesIndex;
	public final static int OFFSETX = 10;
	public final static int OFFSETY = 10;
	public final static int TILE_SHOW_X = 9;
	public final static int TILE_SHOW_Y = 9;
	
	
	
	
	
	protected int x = -10 * 48;
	protected int y = -8 * 48;
	protected int prevX;
	protected int prevY;
	
	public Map(SpriteSheet s) {
		this.s = s;
		init();
	}

	public void init() {

		tilesIndex = new int[][] { 
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 15, 0, 1, 94, 1, 0, 15, 0, 1, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 92, 1, 93, 1, 92, 1, 1, 1, 1, 1, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 1, 0, 11, 0, 90, 0, 0, 0, 0, 0, 91, 0, 11, 0, 1, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 1, 0, 15, 0, 1, 12, 0, 0, 0, 13, 1, 0, 15, 0, 1, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 15, 0, 0, 0, 15, 1, 1, 1, 1, 1, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 1, 0, 15, 0, 1, 12, 0, 0, 0, 13, 1, 0, 15, 0, 1, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 91, 0, 0, 15, 0, 0, 90, 0, 0, 0, 1, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 89, 1, 89, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 15, 1, 15, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 14, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		};
		/*tilesIndex = new int[][] { 
			{0, 0, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 1, 0, 1, 0, 0},
			{0, 0, 0, 0, 1, 0, 1, 0, 0},
			{0, 0, 0, 0, 0, 0, 1, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0},			
		};*/
		initTiles();
		
	}

	

	public void initTiles(){
		
		for (int i = 0; i < tilesIndex.length; i++) {
			
			for (int j = 0; j < tilesIndex[i].length; j++) {
				int tx = j * Tile.WIDTH + x + Map.OFFSETX;
				int ty = i * Tile.HEIGHT + y + Map.OFFSETY;
				Tile t;
				switch (tilesIndex[i][j]) {
				case TileType.BASE_TILE:
					t = new BaseTile(this.s, tx, ty);
					break;
				case TileType.WALL:
					t = new WallTile(this.s, tx, ty);
					break;
				case TileType.CHIP:
					Inventory.getInstance().chips++;
					t = new ChipTile(this.s, tx, ty);
					break;
				case TileType.KEY_YELLOW:
					t = new KeyTile(this.s, tx, ty, Color.YELLOW);
					break;
				case TileType.KEY_GREEN:
					t = new KeyTile(this.s, tx, ty, Color.GREEN);
					break;
				case TileType.KEY_BLUE:
					t = new KeyTile(this.s, tx, ty, Color.BLUE);
					break;
				case TileType.KEY_RED:
					t = new KeyTile(this.s, tx, ty, Color.RED);
					break;
				case TileType.GOAL:
					t = new GoalTile(this.s, tx, ty);
					break;
				case TileType.GOAL_ENTRANCE:
					t = new GoalEntranceTile(this.s, tx, ty);
					break;
				case TileType.LOCK_BLUE:
					t = new LockTile(this.s, tx, ty, Color.BLUE);
					break;
				case TileType.LOCK_GREEN:
					t = new LockTile(this.s, tx, ty, Color.GREEN);				
					break;
				case TileType.LOCK_RED:
					t = new LockTile(this.s, tx, ty, Color.RED);
					break;
				case TileType.LOCK_YELLOW:
					t = new LockTile(this.s, tx, ty, Color.YELLOW);
					break;
				default:
					t = new BaseTile(s, tx, ty);
					break;
				}
				tiles[i][j] = t;
			}
		}
	}
	public void draw(Graphics g) {		
		for (int i = 0; i < tiles.length; i++) {
			for (int j = 0; j < tiles[i].length; j++) {
				if(tiles[i][j].x >= 0 && tiles[i][j].x<Map.TILE_SHOW_X*48){
					if(tiles[i][j].y >= 0 && tiles[i][j].y<TILE_SHOW_Y*48){
						tiles[i][j].draw();	
					}
				}
				
			}
		}		
	}
	
	
	public void moveLeft() {
		if (!isIntersect(Map.MOVE_STEP, 0)) {
			moveMap(Map.MOVE_STEP, 0);
		}
	}

	public void moveRight() {
		if (!isIntersect(-Map.MOVE_STEP, 0)) {			
			moveMap(-Map.MOVE_STEP, 0);
		}
	}

	public void moveUp() {
		if (!isIntersect(0, Map.MOVE_STEP)) {
			moveMap(0, Map.MOVE_STEP);
		}
	}
	public void moveDown(){
		if (!isIntersect(0, -Map.MOVE_STEP)) {
			moveMap(0, -Map.MOVE_STEP);
		}
	}	
	public void moveMap(int x, int y){
		for (int i = 0; i < tiles.length; i++) {
			for (int j = 0; j < tiles[i].length; j++) {	
				tiles[i][j].x += x;
				tiles[i][j].y += y;
			}
		}
	}
	public boolean isIntersect(int moveX, int moveY){
		for (int i = 0; i < tiles.length; i++) {
			for (int j = 0; j < tiles[i].length; j++) {				
				int expectedTileX = tiles[i][j].x + moveX;
				int expectedTileY = tiles[i][j].y + moveY;
									
					if(doOverlap(new Point(expectedTileX, expectedTileY),
							new Point(expectedTileX+Tile.WIDTH-1, expectedTileY+Tile.HEIGHT-1),
							new Point(Player.x, Player.y),
							new Point(Player.x + Tile.WIDTH-1, Player.y+Tile.HEIGHT-1))){
						if(tiles[i][j].accessible == 0){
							return true;	
						}else if(tiles[i][j].accessible == 2){
							tiles[i][j].execute();	
							return true;
						}
						else {
							tiles[i][j].execute();							
							if(Inventory.getInstance().chips == 0){
								makeGoalEntranceAccessible();
								return false;
							}
						}
						
					}	
												
			}
		}	
		return false;
	}
	boolean doOverlap(Point tileTL, Point tileBD, Point playerTL, Point playerBD)
	{	
	    // If one rectangle is on left side of other
	    if (tileTL.x > playerBD.x || playerTL.x > tileBD.x)
	        return false;
	    // If one rectangle is above other
	    if (tileTL.y > playerBD.y || playerTL.y > tileBD.y)
	    	return false;
	    
	    return true;
	}
	
	public void makeGoalEntranceAccessible(){
		for (int i = 0; i < tiles.length; i++) {
			for (int j = 0; j < tiles[i].length; j++) {	
				if(tilesIndex[i][j] == TileType.GOAL_ENTRANCE){
					tiles[i][j].accessible = 1;
				}
			}
		}
	}
	   
}
