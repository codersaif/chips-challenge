package bd.edu.green.app;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SpriteSheet;

import bd.edu.green.tiles.Tile;

public class Player extends Character {
	Boolean moving = false;
	public final static int x = 8 * 48 / 2 + Map.OFFSETX;
	public final static int y = 8 * 48 / 2 + Map.OFFSETY;


	public Player(SpriteSheet s) {
		super(s);
		init(s);

	}

	public void update() {

	}

	public void init(SpriteSheet s) {
		super.init();

		up.addFrame(s.getSubImage(0, 3), duration);
		up.addFrame(s.getSubImage(1, 3), duration);
		up.addFrame(s.getSubImage(2, 3), duration);
		up.addFrame(s.getSubImage(3, 3), duration);

		down.addFrame(s.getSubImage(0, 4), duration);
		down.addFrame(s.getSubImage(1, 4), duration);
		down.addFrame(s.getSubImage(2, 4), duration);
		down.addFrame(s.getSubImage(3, 4), duration);

		left.addFrame(s.getSubImage(4, 3), duration);
		left.addFrame(s.getSubImage(5, 3), duration);
		left.addFrame(s.getSubImage(6, 3), duration);
		left.addFrame(s.getSubImage(7, 3), duration);

		right.addFrame(s.getSubImage(4, 4), duration);
		right.addFrame(s.getSubImage(5, 4), duration);
		right.addFrame(s.getSubImage(6, 4), duration);
		right.addFrame(s.getSubImage(7, 4), duration);

		this.current = up;
	}

	public void move(int direction) {

		switch (direction) {
		case Direction.LEFT:
			this.current = left;
			break;
		case Direction.RIGHT:
			this.current = right;
			break;
		case Direction.UP:
			this.current = up;
			break;
		case Direction.DOWN:
			this.current = down;
			break;
		}
		moving = true;
	}

	public void draw(Graphics g) {
		g.setColor(Color.blue);
		g.drawRect(x, y, Tile.WIDTH, Tile.HEIGHT);
		this.current.draw(x, y);
		if (moving) {
			this.current.start();
		} else {
			this.current.stop();
		}
		moving = false;
	}

}
