package bd.edu.green.app;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import bd.edu.green.tiles.Tile;

public class Game extends StateBasedGame {

	public Game(String name) {
		super(name);
	}

	public static void main(String[] args) throws SlickException {
		AppGameContainer container = new AppGameContainer(new Game("Chip's Challenge"));
		container.setTargetFrameRate(10);
		container.setDisplayMode((Map.TILE_SHOW_X + Inventory.TILE_SHOW_X) *
				Tile.WIDTH + Map.OFFSETX * 3, 9 * Tile.HEIGHT + 20, false);
		container.start();
	}

	@Override
	public void initStatesList(GameContainer arg0) throws SlickException {
		this.addState(new Engine());
	}

}
